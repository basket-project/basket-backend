package basket.basket_backend.service;

import basket.basket_backend.dto.ProductDTO;
import basket.basket_backend.exception.ProductNotFoundException;
import basket.basket_backend.exception.QuantityInvalidException;
import basket.basket_backend.model.Product;
import basket.basket_backend.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;
    public ProductServiceImpl(ProductRepository productRepository, ModelMapper modelMapper) {
        this.productRepository = productRepository;
        this.modelMapper    = modelMapper;
    }
    @Override
    public List<ProductDTO> findAll() {
        List<ProductDTO> products = productRepository.findAll().stream()
                .map(product -> modelMapper.map(product, ProductDTO.class))
                .collect(Collectors.toList());
        return products;
    }

    @Override
    public Product decrementQuantityAvailable(Product product, int quantity) {
        product.setStock(product.getStock() - quantity);
        if (product.getStock() < 0)
            throw new QuantityInvalidException();
        return productRepository.save(product);
    }

    @Override
    public ProductDTO findById(Long id) {
        Product product         = productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
        ProductDTO productDTO   = modelMapper.map(product, ProductDTO.class);
        return  productDTO;
    }
}
