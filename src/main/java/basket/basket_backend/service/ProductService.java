package basket.basket_backend.service;

import basket.basket_backend.dto.ProductDTO;
import basket.basket_backend.model.Product;
import java.util.List;

public interface ProductService {
    List<ProductDTO> findAll();
    Product decrementQuantityAvailable (Product product, int quantity);
    ProductDTO findById(Long id);
}
