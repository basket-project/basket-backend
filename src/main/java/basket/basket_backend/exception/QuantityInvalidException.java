package basket.basket_backend.exception;

public class QuantityInvalidException extends RuntimeException {
    public QuantityInvalidException() {
        super("Quantity invalid");
    }
}
