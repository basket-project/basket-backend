insert into product (name, description, price, image, stock) values ('Coca Cola', 'Boisson gazeuses', 10.5, '/images/coca-cola.jpeg', 150);
insert into product (name, description, price, image, stock) values ('Pampers', 'Couche bebe', 5.2, '/images/pampers.jpeg', 10);
insert into product (name, description, price, image, stock) values ('Riz long grain', 'Riz long grain 5kg', 4.5, '/images/riz-long-grain.jpeg', 54);
insert into product (name, description, price, image, stock) values ('Nike super', 'Chaussure de sport', 49.99, '/images/shoes.jpeg', 50);