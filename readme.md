# Carrefour Java kata

## Consignes

Les instructions suivantes vous permettront d'obtenir une copie du projet opérationnel en local à des fins de développement et de test.

## Fonctionnalités

> - Afficher la liste des produits disponibles(avec stock disponible, prix...).
> - Ajouter, modifier ou retirer des produits dans un panier.
> - Retirez des prodtuits du panier si la date dépasse du 24h

### Technologies utilisées:
- Spring Boot 3.2+
- Java 21
- git

### Configuration

```
cd basket-backend
./gradlew bootRun
```

## Auteur

**Iangolana Riantsoa Ramelson** - [riantsoaramelson@gmail.com](mailto:riantsoaramelson@gmail.com)